package main

import "fmt"

const (
    a = iota
    _ = iota
    c = iota
    x = iota
    _ = iota
    z = iota
    q = iota * 10
    w
    e
)

func main(){
    fmt.Println(a, c, x, z, q, w, e)
}
