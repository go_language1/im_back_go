package main

import "fmt"

var x int
var y bool
var z string
var t float64

func main(){
    fmt.Printf("%v, %T\n", x, x)
    fmt.Printf("%v, %T\n", y, y)
    fmt.Printf("%v, %T\n", z, z)
    fmt.Printf("%v, %T\n", t, t)
}
