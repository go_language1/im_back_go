package main

import "fmt"

func main(){
	equal := 10 == 10
	not_equal := 10 != 5
	smaller_or_equal := 10 <= 5
	smallerthat := 10 < 5
	bigger_or_equal := 10 >= 5
	biggerthat := 10 > 5


	fmt.Printf("equal: %v\n", equal)
	fmt.Printf("different : %v\n", not_equal)
	fmt.Printf("smaller or equal: %v\n", smaller_or_equal)
	fmt.Printf("smaller: %v\n", smallerthat)
	fmt.Printf("bigger or equal: %v\n", bigger_or_equal)
	fmt.Printf("bigger: %v\n", biggerthat)
}