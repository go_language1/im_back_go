package main

import (
	"fmt"
)

func main(){
	whoIsInOfficeToday := "Taina"

	switch whoIsInOfficeToday {
		case "Zezinho":
			fmt.Println("Zezinho")
		case "Taina":
			fmt.Println("Taina")
			fallthrough
		case "Marquinhos":
			fmt.Println("Marquinhos")
		default:
			fmt.Println("no one")
	}
}