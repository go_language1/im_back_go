package main

import (
	"fmt"
)

func main(){
	x := 0

	//for x < 10 {
	//	fmt.Println("X's smaller")
	//	x++
	//}

	for {
		if x < 10 {
			x++
			fmt.Printf("X's smaller = %v\n", x)
		} else {
			fmt.Printf("X's bigger %v\n", x)
			break
		}
	}
	fmt.Println("Loop is stopped")
}