package main

import (
	"fmt"
)

func main(){
	if x := 1000; x > 100 {
		fmt.Println("X is bigger")
	} else if x < 10 {
		fmt.Println("X is smaller")
	} else {
		fmt.Println("X isn't small than 10 neither big that 100!")
	}
}