package main

import (
	"fmt"
)

func main(){
	y := 10

	// initial value; condition	
	if x := 102; x < 100 {
		fmt.Println("X -> Hello, playground!")
	}

	// without initial value
	if y < 100 {
		fmt.Println("Y -> Hello, playground!")
	}
}