package main

import "fmt"

type hot int
var x hot
var y int 

func main(){
    fmt.Printf("%v, %T\n", x , x)
    x = 42
    fmt.Println(x)
    fmt.Println("-----------")
    y = int(x)
    fmt.Printf("%v, %T\n", y , y)
}
